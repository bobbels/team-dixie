#include "zmq.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <time.h>

#define SUB_INDEX 0
#define STDIN_INDEX 1
#define MAX_SIZE 64

static char username[MAX_SIZE] = {0};
static char ip[MAX_SIZE] = {0};
static char color[MAX_SIZE] = {0};

#define PUSH_INDEX 2

//  Convert C string to 0MQ string and send to socket
static int s_send (void *socket, char *string) {
	int size = zmq_send (socket, string, strlen(string), 0);
	return size;
}

static int s_sendmore (void *socket, char *string) {
	int size = zmq_send(socket, string, strlen(string), ZMQ_SNDMORE);
	return size;
}

static int draw_cow(){
        int res = 0;
        struct timespec ts_asked;
        struct timespec ts_real;
        ts_asked.tv_sec = 0;
        ts_asked.tv_nsec = 5000000;

        FILE *fptr;
        char c = 'a';
        // Open file
        fptr = fopen("../gifs/cow.vt100gif", "r");
        if (fptr == NULL)
        {
                printf("Cannot open file \n");
                exit(0);
        }

        // Read contents from file
        c = fgetc(fptr);
        while (c != EOF)
        {
                printf ("%c", c);
                c = fgetc(fptr);
                if (c == 'A') {
                        res = nanosleep(&ts_asked, &ts_real);
                }
        }
        fclose(fptr);
        return 0;
}


static char *s_recv(void *socket)
{
	char buffer [256];
	int size = zmq_recv(socket, buffer, 255, 0);
	if (size == -1) {
		return NULL;
	}
	buffer[size] = '\0';
	return strndup (buffer, sizeof(buffer) - 1);
}

static void handle_poll(zmq_pollitem_t *items, void *subscriber, void *pusher)
{
	if (items[SUB_INDEX].revents & ZMQ_POLLIN) {
		char *address = s_recv(subscriber);
		char *contents = s_recv(subscriber);
		
		if (strstr(contents, "<cow>") != NULL) {
			draw_cow();
		}
		printf("[%s] %s",address, contents);
		//  Read message contents
		free(contents);
		free(address);
	}

	if (items[STDIN_INDEX].revents & ZMQ_POLLIN) {
		char buf[256] = {'\0'};
		char format[300] = {'\0'};
		int ret = read(0, buf, 256);
		if(ret < 0) {
			err(ret, "read stdin");
		}

		snprintf(format, 300, "\e[%sm%s: \e[39m%s", color, username, buf);

		s_sendmore(pusher, "B");
		s_send(pusher, format);
	}
}


static void startup(char *ip_addr, char *name, char *colorcode)
{
	printf("Give a valid ip address of the server:\n");
	scanf("%s", ip_addr);
	printf("Choose a username:\n");
	scanf("%s", name);
	printf("Choose a colorcode:\n");
	scanf("%s", colorcode);
}


int main(int argc, char *argv[])
{
	if (argc == 1) {
		startup(ip, username, color);
	} else if (argc == 4) {
		strcpy(ip, argv[1]);
		strcpy(username, argv[2]);
		strcpy(color, argv[3]);
	} else {
		printf("Usage: %s <ip> <username> <color code>\n", argv[0]);
		return -1;
	}

	//  Prepare our context and subscriber
	void *context = zmq_ctx_new();
	void *subscriber = zmq_socket(context, ZMQ_SUB);

	char buffer[26] = {0};
	snprintf(buffer, 26, "tcp://%s:5563", ip);
	zmq_connect(subscriber, buffer);
	zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, "B", 1);

	// prepare the publisher
	void *pusher = zmq_socket(context, ZMQ_PUSH);
	snprintf(buffer, 26, "tcp://%s:5564", ip);
	zmq_connect(pusher, buffer);

	zmq_pollitem_t items[2];

	items[SUB_INDEX].socket = subscriber;
	items[SUB_INDEX].events = ZMQ_POLLIN;

	items[STDIN_INDEX].socket = NULL;
	items[STDIN_INDEX].fd = 0;
	items[STDIN_INDEX].events = ZMQ_POLLIN;

	while(1) {
		//zmq polling
		int rc = zmq_poll(items, 2, -1);
		if (rc < 0) {
			printf("error\n");
		} else if (rc == 0) {
			printf("timeout\n");
		} else if(rc > 0) {
			handle_poll(items, subscriber, pusher);
		}
	}

	zmq_close (subscriber);
	zmq_close (pusher);
	zmq_ctx_destroy (context);
	return 0;
}

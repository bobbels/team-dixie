#include <stdio.h>

#include "../lib/zhelpers.h"

int main(void)
{
	//  Prepare our context and sockets
	void *context = zmq_ctx_new();

	void *pull_frontend = zmq_socket(context, ZMQ_PULL);
	void *pub_frontend = zmq_socket(context, ZMQ_PUB);

	zmq_bind(pull_frontend, "tcp://*:5564");
	zmq_bind(pub_frontend, "tcp://*:5563");

	zmq_pollitem_t item1;
	item1.socket = pull_frontend;
	item1.events = ZMQ_POLLIN;

	//  Initialize poll set
	zmq_pollitem_t items[1] = {item1};

	//  Switch messages between sockets
	while(1) {

		zmq_poll(items, 1, -1);

		if(items[0].revents & ZMQ_POLLIN) {
			char *room = s_recv(pull_frontend);
			char *msg = s_recv(pull_frontend);
			s_sendmore(pub_frontend, room);
			s_send(pub_frontend, msg);
			free(room);
			free(msg);
		}
	}

	//  We never get here, but clean up anyhow
	zmq_close(pull_frontend);
	zmq_close(pub_frontend);
	zmq_ctx_destroy(context);
	return 0;
}


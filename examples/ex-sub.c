#include "zmq.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char * s_recv(void *socket)
{
	char buffer [256];
	int size = zmq_recv (socket, buffer, 255, 0);
	if (size == -1) {
		return NULL;
	}
	buffer[size] = '\0';
	return strndup (buffer, sizeof(buffer) - 1);
}

int main (void)
{
    //  Prepare our context and subscriber
    void *context = zmq_ctx_new ();
    void *subscriber = zmq_socket (context, ZMQ_SUB);
    zmq_connect (subscriber, "tcp://localhost:5563");
    // zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, "A", 1);
    zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, "B", 1);

    while (1) {
        //  Read envelope with address
        char *address = s_recv (subscriber);
        //  Read message contents
        char *contents = s_recv (subscriber);
        printf ("[%s] %s\n", address, contents);
        free (address);
        free (contents);
    }
    //  We never get here, but clean up anyhow
    zmq_close (subscriber);
    zmq_ctx_destroy (context);
    return 0;
}
